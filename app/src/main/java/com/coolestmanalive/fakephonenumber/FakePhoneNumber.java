package com.coolestmanalive.fakephonenumber;

/**
 * Created by j on 10/29/14.
 */

import java.lang.reflect.Method;


import java.lang.reflect.Modifier;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.HashSet;
import java.util.HashMap;

import android.content.Context;
import android.app.AndroidAppHelper;
import android.location.Location;
import android.location.LocationListener;
import android.os.SystemClock;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;
import de.robv.android.xposed.XC_MethodHook;
import java.util.Set;
import java.util.Random;

import static de.robv.android.xposed.XposedHelpers.findClass;
import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;

public class FakePhoneNumber implements IXposedHookLoadPackage
{

    private boolean mTelephonyManagerHooked = false;
    private boolean mTWifiManagerHooked = false;

    private static Settings settings = new Settings();

    @Override
    public void handleLoadPackage(LoadPackageParam lpparam) throws Throwable
    {
        findAndHookMethod( "android.net.wifi.WifiInfo", lpparam.classLoader, "getMacAddress", new XC_MethodHook()
        {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable
            {
                settings.reload();
                if( settings.isStarted() )
                {
                    param.setResult( settings.getWifiMac() );
                }

            }
        });

        findAndHookMethod( "android.bluetooth.BluetoothAdapter", lpparam.classLoader, "getAddress", new XC_MethodHook()
        {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable
            {
                settings.reload();
                if( settings.isStarted() )
                {
                    param.setResult( settings.getBTMac() );
                }

            }
        });

        findAndHookMethod( "android.telephony.TelephonyManager", lpparam.classLoader, "getLine1Number", new XC_MethodHook()
        {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable
            {
                settings.reload();
                if( settings.isStarted() )
                {
                    param.setResult(settings.getPhoneNumber());
                }

            }
        });

        findAndHookMethod( "android.telephony.TelephonyManager", lpparam.classLoader, "getDeviceId", new XC_MethodHook()
        {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable
            {
                settings.reload();
                if( settings.isStarted() )
                {
                    param.setResult(settings.getIMEI());
                }

            }
        });
    }
}
