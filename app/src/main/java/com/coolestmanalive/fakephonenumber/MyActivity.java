package com.coolestmanalive.fakephonenumber;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;


public class MyActivity extends Activity {


    private Switch onOffSwitch;
    private EditText editPhone;
    private EditText editIMEI;
    private EditText editWifimac;
    private EditText editBtmac;


    private Settings settings = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        // find ui elements
        onOffSwitch = (Switch)findViewById(R.id.switchOnOff);
        editPhone = (EditText)findViewById(R.id.editTextPhone);
        editIMEI = (EditText)findViewById(R.id.editTextImei);
        editWifimac = (EditText)findViewById(R.id.editTextWifimac);
        editBtmac = (EditText)findViewById(R.id.editTextBtmac);

        onOffSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                UpdateSettings();
            }
        });

        ((Button)findViewById(R.id.buttonSave)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                UpdateSettings();
            }
        });


        settings = new Settings( getApplicationContext() );
        if( !settings.getPhoneNumber().isEmpty() )
        {
            editPhone.setText( settings.getPhoneNumber() );
        }
        if( !settings.getIMEI().isEmpty() )
        {
            editIMEI.setText( settings.getIMEI() );
        }
        if( !settings.getWifiMac().isEmpty() )
        {
            editWifimac.setText( settings.getWifiMac() );
        }
        if( !settings.getBTMac().isEmpty() )
        {
            editBtmac.setText( settings.getBTMac() );
        }

        onOffSwitch.setChecked( settings.isStarted() );
    }

    private void UpdateSettings()
    {
        boolean enabled = onOffSwitch.isChecked();
        settings.update(
                editPhone.getText().toString(),
                editIMEI.getText().toString(),
                editWifimac.getText().toString(),
                editBtmac.getText().toString(),
                enabled );
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/
}
