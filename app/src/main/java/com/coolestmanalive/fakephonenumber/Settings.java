package com.coolestmanalive.fakephonenumber;

/**
 * Created by j on 10/29/14.
 */

import android.content.SharedPreferences;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import de.robv.android.xposed.XSharedPreferences;

public class Settings
{

    private static boolean start = true;
    final static String DEFAULT_PHONE_NUMBER = "17068675309";
    final static String DEFAULT_IMEI = "9988776655443322";
    final static String DEFAULT_WIFI_MAC = "11:22:33:44:55:66";
    final static String DEFAULT_BT_MAC = "11:22:33:44:55:66";
    //private Context context = null;
    private XSharedPreferences xSharedPreferences = null;
    private SharedPreferences sharedPreferences = null;

    public Settings()
    {
        xSharedPreferences = new XSharedPreferences( "com.coolestmanalive.fakephonenumber", "settings");
        // xSharedPreferences.makeWorldReadable();
    }

    public Settings(Context context)
    {
        sharedPreferences = context.getSharedPreferences( "settings", Context.MODE_WORLD_READABLE );
        //this.context = context;
    }

    public String getPhoneNumber() {
        if (sharedPreferences != null)
            return sharedPreferences.getString( "phonenumber", DEFAULT_PHONE_NUMBER );
        else if (xSharedPreferences != null)
            return xSharedPreferences.getString( "phonenumber", DEFAULT_PHONE_NUMBER );
        return DEFAULT_PHONE_NUMBER;
    }

    public String getWifiMac()
    {
        if (sharedPreferences != null)
            return sharedPreferences.getString("wifimac", DEFAULT_WIFI_MAC);
        else if (xSharedPreferences != null)
            return xSharedPreferences.getString("wifimac", DEFAULT_WIFI_MAC);
        return DEFAULT_WIFI_MAC;
    }

    public String getBTMac()
    {
        if (sharedPreferences != null)
            return sharedPreferences.getString("btmac", DEFAULT_BT_MAC);
        else if (xSharedPreferences != null)
            return xSharedPreferences.getString("btmac", DEFAULT_BT_MAC);
        return DEFAULT_BT_MAC;
    }

    public String getIMEI()
    {
        if (sharedPreferences != null)
            return sharedPreferences.getString("imei", DEFAULT_IMEI);
        else if (xSharedPreferences != null)
            return xSharedPreferences.getString("imei", DEFAULT_IMEI);
        return DEFAULT_IMEI;
    }

    public boolean isStarted() {
        if (sharedPreferences != null)
            return sharedPreferences.getBoolean("start", start);
        else if (xSharedPreferences != null)
            return xSharedPreferences.getBoolean("start", start);
        return start;
    }

    public void update(
            String phoneNumber,
            String imei,
            String wifimac,
            String btmac,
            boolean start )
    {
        Log.d( "foo", "save prefs: " + phoneNumber + imei + wifimac + btmac );
        SharedPreferences.Editor prefEditor = sharedPreferences.edit();
        prefEditor.putString("phonenumber", phoneNumber );
        prefEditor.putString("wifimac", wifimac);
        prefEditor.putString("btmac", btmac);
        prefEditor.putString("imei", imei);
        prefEditor.putBoolean("start",   start);
        prefEditor.apply();
    }

    /*settings.update(
            editPhone.getText().toString(),
    editIMEI.getText().toString(),
    editWifimac.getText().toString(),
    editBtmac.getText().toString(),
    enabled );*/

    public void reload()
    {
        xSharedPreferences.reload();
    }
}

